
from parser import *
from plot_defaults import *
import numpy as np
from operator import itemgetter
import sys
import argparse
from scipy.interpolate import interp1d
import math

parser = argparse.ArgumentParser()
parser.add_argument('--files', nargs="+", required=True)
parser.add_argument('--field', default=8, type=int)
parser.add_argument('--out', default=None)
FLAGS = parser.parse_args()
COLOURS = ['green', 'blue', 'yellow', 'red', 'orange']

fields = ['lsewma', 'lrewma', 'lratio',
          'hsewma', 'hrewma', 'hratio',
          'winc', 'wmul', 'ipg']
units = ['', '', '', '', '', '',
         '', '', 'ms']

def partial_sums(data):
    ret = []
    total = 0.0
    for x in data:
        total += float(x)
        ret.append(total)
    return ret

def plot_histogram(ax, data, **kwargs):
    #n, bins, patches = plt.hist(data, 50, alpha=0.3)
    hist, bins = np.histogram(data, 20)
    xs = (bins[:-1] + bins[1:])/2.0
    ys = hist
    width = (bins[1] - bins[0])
    ax.bar(xs, ys, width=width, **kwargs)
    ax.set_ylabel("Count")

def plot_cdf(ax, data, **kwargs):
    hist, bins = np.histogram(data, 20)
    xs = (bins[:-1] + bins[1:])/2.0
    ys = partial_sums(hist) / sum(hist)
    ax.step(xs, ys, **kwargs)
    #f = interp1d(xs, ys, kind='quadratic')
    #ax.plot(xs, f(xs), color='black')
    ax.set_ylabel("fractiles")
    return xs, ys

print 'plotting', fields[FLAGS.field]

fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax2 = ax1.twinx()

num_samples = []
cdfs = []

def step_interpol(xs, ys):
    def f(x):
        value = 0
        for _x, _y in zip(xs, ys):
            if _x <= x:
                value = _y
            else:
                break
        return value
    return f

for col,file in zip(COLOURS, FLAGS.files):
    data = parse_file(file)
    print 'plotting', file
    data = map(itemgetter(FLAGS.field), data)
    plot_histogram(ax1, data, color=col, alpha=0.3, label=file)
    xs, ys = plot_cdf(ax2, data, color=col)
    num_samples.append(len(data))
    cdfs.append((xs, ys))

def two_sample_ks_test():
    points = np.linspace(0, max(map(lambda (xs,ys): max(xs), cdfs)), 100)
    values = []
    if len(cdfs) != 2:
        print 'error: cannot do KS test for more than 1 pair.'
        return
    for (xs,ys) in cdfs:
        f = step_interpol(xs,ys)
        values.append(map(f, points))
    diffs = np.abs(np.array(values[0]) - np.array(values[1]))
    KS = max(diffs)
    print 'KS stat: %.3f' % KS
    print 'diffs:', diffs
    criticals = [1.22, 1.36, 1.48, 1.63, 1.73, 1.95]
    alphas = [0.1, 0.05, 0.025, 0.01, 0.005, 0.001]
    total_samples = sum(num_samples)
    product_samples = reduce(lambda x, y: x*y, num_samples, 1)
    print 'sum_samples: %d, product_samples: %d' % (total_samples, product_samples)
    print 'Null hypothesis: two samples come from the SAME distribution.'
    for alpha, crit in zip(alphas, criticals):
        crit = crit * (math.sqrt(float(total_samples)/product_samples))
        print 'Critical value at level %.3f = %.3f' % (alpha, crit),
        if KS > crit:
            print '...reject null hypothesis'
        else:
            print '...ACCEPT null hypothesis'

two_sample_ks_test()

ax1.set_xlabel("%s (%s)" % (fields[FLAGS.field], units[FLAGS.field]))
ax1.legend(loc="upper left", fancybox=False, prop={'size': 12})

if FLAGS.out:
    print 'saving to', FLAGS.out
    plt.savefig(FLAGS.out)
else:
    plt.show()
