
from parser import *
from plot_defaults import *
from mpl_toolkits.mplot3d import Axes3D

import numpy as np
from operator import itemgetter
import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--files', nargs="+", required=True)
parser.add_argument('--field', default=8, type=int)
parser.add_argument('--out', default=None)
parser.add_argument('--inverse', default=False, action="store_true")
parser.add_argument('--colours', default='gbyro')
parser.add_argument('--exclude', default=False, action="store_true")
parser.add_argument('--range', default=None)

FLAGS = parser.parse_args()
COLOURS = FLAGS.colours

fields = ['lsewma', 'lrewma', 'lratio',
          'hsewma', 'hrewma', 'hratio',
          'winc', 'wmul', 'ipg']
units = ['', '', '', '', '', '',
         '', '', 'ms']

xl, xh, yl, yh, zl, zh = [0, 1e10, 0, 1e10, 0, 1e10]
if FLAGS.range:
    xl, xh, yl, yh, zl, zh = map(float, FLAGS.range.split(','))

def plot_regions(ax, data, **kwargs):
    if FLAGS.exclude:
        data = filter(lambda lst: 16384 not in lst, data)
    ax.set_xlabel("sewma")
    ax.set_ylabel("rewma")
    ax.set_zlabel("ratio")
    def in_range(a,b, x,y):
        return a >= x and b <= y
    def in_xrange(a,b):
        return in_range(a, b, xl, xh)
    def in_yrange(a,b):
        return in_range(a, b, yl, yh)
    def in_zrange(a,b):
        return in_range(a, b, zl, zh)

    filtered_data = []
    for whisker in data:
        whisker = map(lambda e: min(e, 10), whisker)
        l_sewma, l_rewma, l_ratio, h_sewma, h_rewma, h_ratio, \
            w_inc, w_mul, w_inter = whisker
        if in_xrange(l_sewma, h_sewma) and \
                in_yrange(l_rewma, h_rewma) and \
                in_zrange(l_ratio, h_ratio):
            print whisker
            filtered_data.append(whisker)
    data = filtered_data

    if FLAGS.inverse:
        max_field_value = max(map(lambda lst: 1.0/lst[FLAGS.field],
                                  data))
    else:
        max_field_value = max(map(lambda lst: lst[FLAGS.field],
                                  data))

    for whisker in data:
        l_sewma, l_rewma, l_ratio, h_sewma, h_rewma, h_ratio, \
            w_inc, w_mul, w_inter = whisker
        if FLAGS.inverse:
            alpha = 0.5 * (1.0/whisker[FLAGS.field]) / max_field_value
        else:
            alpha = 0.5 * whisker[FLAGS.field] / max_field_value
        ax.bar3d(l_sewma, l_rewma, l_ratio,
                 h_sewma-l_sewma, h_rewma-l_rewma, h_ratio-l_ratio,
                 alpha=alpha,
                 **kwargs)

    return data

def plot_2d(ax, data, **kwargs):
    if FLAGS.inverse:
        max_field_value = max(map(lambda lst: 1.0/lst[FLAGS.field],
                                  data))
    else:
        max_field_value = max(map(lambda lst: lst[FLAGS.field],
                                  data))

    for whisker in data:
        l_sewma, l_rewma, l_ratio, h_sewma, h_rewma, h_ratio, \
            w_inc, w_mul, w_inter = whisker
        if FLAGS.inverse:
            alpha = 0.9 * (1.0/whisker[FLAGS.field]) / max_field_value
        else:
            alpha = 0.9 * whisker[FLAGS.field] / max_field_value
        ax.bar(l_sewma, h_rewma-l_rewma, h_sewma-l_sewma,
               l_rewma,
               alpha=alpha,
               **kwargs)
    ax2.set_xlabel("sewma")
    ax2.set_ylabel("rewma")
    ax2.set_title(fields[FLAGS.field] + " - rtt-ratio = 1")
    return data

print 'plotting', fields[FLAGS.field]

fig = plt.figure()
fig2 = plt.figure()
ax1 = fig.add_subplot(1, 1, 1, projection='3d')
ax2 = fig2.add_subplot(1, 1, 1)

for col,file in zip(COLOURS, FLAGS.files):
    data = parse_file(file)
    print 'plotting', file
    filtered_data = plot_regions(ax1, data, color=col, lw=0.05, label=file)
    plot_2d(ax2, filtered_data)

if FLAGS.range:
    xl, xh, yl, yh, zl, zh = map(float, FLAGS.range.split(','))
    ax1.set_xlim((xl, xh))
    ax1.set_ylim((yl, yh))
    ax1.set_zlim((zl, zh))

#plt.tight_layout()
plt.title(fields[FLAGS.field])
#ax1.legend(loc="upper left", prop={'size': 12})
if FLAGS.out:
    print 'saving (both 3d and 2d)', FLAGS.out
    fig.savefig(FLAGS.out)
    fig2.savefig(FLAGS.out + "-2d.png")
else:
    plt.show()
