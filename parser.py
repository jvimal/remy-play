

def parse_file(fname):
	lines = open(fname).readlines()
	ret = []
	for line in lines:
		ret.append(map(float, line.strip().split(',')))
	return ret
