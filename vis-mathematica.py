import sys
from parser import *

opacity = 0.5

def vis_one(whisker):
	whisker = map(lambda w: min(w, 10), whisker)
	l_sewma, l_rewma, l_ratio, h_sewma, h_rewma, h_ratio, \
	w_inc, w_mul, w_inter = whisker
	col = 'RGBColor[RandomReal[], RandomReal[], RandomReal[]]'

	if w_inter < 2:
		c = 'Red'
		opacity = 0.3
	else:
		c = 'Green'
		opacity = 0.1

	s = c + ","
	s += 'Opacity[%s],' % opacity
	s += 'Cuboid[{%s,%s,%s},{%s,%s,%s}]' % (l_sewma, l_rewma, l_ratio, h_sewma, h_rewma, h_ratio)

	return s

fname = sys.argv[1]
whiskers = parse_file(fname)
#print vis_one(whiskers[0])

#whiskers = filter(lambda lst: 16384 not in lst, whiskers)
vis = map(vis_one, whiskers)
joined = ','.join(vis)
print 'Graphics3D[{%s}, Axes->True, AxesLabel->{sewma, rewma, ratio}]' % joined

