#include <stdio.h>
#include <vector>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "dna.pb.h"

bool pretty = true;

std::string mem_range_str(const RemyBuffers::MemoryRange &domain)
{
	char buff[1000];
	RemyBuffers::Memory lower, upper;
	lower = domain.lower();
	upper = domain.upper();
	if (pretty) {
	sprintf(buff, "(snd,rec,ratio): (%.3f,%.3f,%.3f -> %.3f,%.3f,%.3f)",
		lower.rec_send_ewma(), lower.rec_rec_ewma(), lower.rtt_ratio(),
		upper.rec_send_ewma(), upper.rec_rec_ewma(), upper.rtt_ratio());
	} else {
	sprintf(buff, "%.3f,%.3f,%.3f,%.3f,%.3f,%.3f",
		lower.rec_send_ewma(), lower.rec_rec_ewma(), lower.rtt_ratio(),
		upper.rec_send_ewma(), upper.rec_rec_ewma(), upper.rtt_ratio());
	}
	return buff;
}

void print(const RemyBuffers::WhiskerTree &tree) {
	if (tree.has_leaf()) {
		RemyBuffers::Whisker leaf = tree.leaf();
		RemyBuffers::MemoryRange domain = tree.domain();
		if (pretty) {
		printf("%s -> (w+%d, w*%.3f, inter:%.3fms)\n",
			mem_range_str(domain).c_str(),
			leaf.window_increment(),
			leaf.window_multiple(),
			leaf.intersend());
		} else {
		printf("%s,%d,%.3f,%.3f\n", mem_range_str(domain).c_str(),
			leaf.window_increment(),
			leaf.window_multiple(),
			leaf.intersend());
		}
	} else {
		for (const auto &x: tree.children()) {
			print(x);
		}
	}
}

int main(int argc, char *argv[]) {
	RemyBuffers::WhiskerTree tree;
	int fd = open(argv[1], O_RDONLY);
	if (argc > 2)
		pretty = false;
	if (fd < 0)
		exit(printf("Cannot open %s\n", argv[1]));
	if (!tree.ParseFromFileDescriptor(fd))
		exit(printf("Cannot parse %d\n",fd));
	print(tree);
	return 0;
}
